# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 15:21:29 2018

@author: owen
"""

import numpy as np
import matplotlib.pyplot as plt
import entropy

#%% Timeseries
f,ax = plt.subplots(1,3,figsize=(11,4.25))
x = np.zeros((256,1))
ax[0].plot(x)
H = entropy.entropy(x)
ax[0].set_xlabel('H = {0:.2f}  nats'.format(H))
ax[0].set_title('Flat');

x = np.random.uniform(low=0,high=2,size=(256,1)).astype(int)
ax[1].plot(x)
H = entropy.entropy(x)
ax[1].set_xlabel("H = {0:.2f} nats, {1:.2f} bits".format(H,H/np.log(2)));
ax[1].set_title('Uniformly dist., 1 bit');


x = np.random.uniform(low=0,high=10,size=(256,1)).astype(int)
ax[2].plot(x)
H = entropy.entropy(x)
ax[2].set_xlabel("H = {0:.2f} nats, {1:.2f} dits".format(H,H/np.log(10)));
ax[2].set_title('Uniformly dist., Decimal');

#%% Synthetic Images
f,ax = plt.subplots(1,3,figsize=(11,4.25))

X = np.zeros((256,256,3))
H = entropy.iment(X)
ax[0].imshow(X)
ax[0].set_xlabel('H = {0:.2f}  nats'.format(H))
ax[0].set_title('Flat image');

X = np.random.uniform(low=0,high=256,size=(256,256)).astype(np.uint8)
H = entropy.iment(X)
ax[1].imshow(X,cmap='gray')
ax[1].set_xlabel("H = {0:.2f} nats, {1:.2f} bims".format(H,H/np.log(256)));
ax[1].set_title('Uniformly dist., 8bit B&W');

X = np.random.uniform(low=0,high=256,size=(1024,1024,3)).astype(np.uint8)
H = entropy.iment(X)
ax[2].imshow(X)
ax[2].set_xlabel("H = {0:.2f} nats, {1:.2f} cims".format(H,H/np.log(256**3)));
ax[2].set_title('Uniformly dist., 8bit RGB');

#NOTE with this example:
#the cims unit should normalize a unfiormly distributed 8bit color image to 1
#however, the size of the image used here is much too small achieve this.
#it may make sense to normalize by the size of the image in the future

#%% Real Image

#load image
I = plt.imread('./test-images/test2.jpg')

#Tile Image
W = 256 #tile width
step = 128 #tile step
XX = []
for jj in np.arange(0,I.shape[0]-W,step):
    for ii in np.arange(1,I.shape[1]-W,step):
        XX.append(I[jj:jj+W,ii:ii+W])
        
HH = entropy.iment_batch(XX,conversion=None)
    
#HBINS = [(0,1),(1,2),(2,3),(3,4),(4,5),(5,6)]
HBINS = [(0,2),(2,4),(4,6),(6,8),(8,10)]
#HBINS = [(6,7),(7,8),(8,9),(9,10),(10,11)]
for hbin in HBINS:
    #select random 5 images with entropy in hbin
    inds = np.where((HH>=hbin[0]) & (HH<hbin[1]))[0]
    inds = np.random.permutation(inds)[:5]

    f,ax = plt.subplots(1,5,figsize=(12,3))
    for i,ind in enumerate(inds):
        ax[i].imshow(XX[ind])
        ax[i].set_xlabel('H = {0:.2f}  nats'.format(HH[ind]))
        ax[i].set_xticks([]); ax[i].set_yticks([])
    f.tight_layout()
#    f.savefig('entropy_examples_color_{}-{}.pdf'.format(hbin[0],hbin[1]))